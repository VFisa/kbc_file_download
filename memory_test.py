import os
import psutil

status = psutil.virtual_memory()
print(status)

total = int(str(status.total))/1048576
available = int(str(status.available))/1048576
free = int(str(status.free))/1048576

print(total, available, free)