__author__ = 'Martin Fiser'
__credits__ = 'Martin Fiser, 2017, Twitter: @VFisa'

# Import Libraries
import pip
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'psutil'])
import os
import requests
import pandas as pd
import sys
from urlparse import urlparse
from os.path import splitext, basename
import gc
import psutil
import logging
import glob
from keboola import docker



# Environment setup
cfg = docker.Config('/data/')
params = cfg.get_parameters()
year_start = cfg.get_parameters()["year_start"]
year_end = cfg.get_parameters()["year_end"]
fileout = "/data/out/tables/data.csv"
logging.basicConfig(filename='python_job.log',level=logging.DEBUG)
#print(script_path)


def get_file(link):
    """
    Request file from url, save it as a filename from URL link
    """

    ## figure out filename
    disassembled = urlparse(link)
    filename, file_ext = splitext(basename(disassembled.path))
    name = (filename+file_ext)
    #print(name)

    ## Download file and save it as filename
    with open(name, 'wb') as temp_file:
        response = requests.get(link)
        if not response.ok:
            logging.error("File "+link+" not available.")
            name = ""
        else:
            logging.info("File "+name+" saved temporarily.")
            temp_file.write(response.content)
    del response
    temp_file.close()
    
    return name


def assemble_data():
    """
    Go through the folder and get all files saved by requests
    USING GLOB
    """

    try:
        file_list = glob.glob("*.csv")

        header_saved = False
        with open(fileout, "wb") as fout:
            for filename in file_list:
                with open(filename) as fin:
                    header = next(fin)
                    if not header_saved:
                        fout.write(header)
                        header_saved = True
                    for line in fin:
                        fout.write(line)
        logging.info("Final file produced.")
    except:
        logging.error("Could not produce final output file.")


def link_generator(link_part, file_type):
    """
    Generate the links based on mask.
    In this case its pretty fixed, can be edited/elaborated on later
    """

    link_gen = []
    year_part = list(range(int(year_start),int(year_end)))
    month_part = list(range(01,13))

    ## generate list of files
    for a in year_part:
        for b in month_part:
            link = link_part+str(a)+"-"+str(b).zfill(2)+"."+file_type
            link_gen.append(link)

    return link_gen


link_part = "https://s3.amazonaws.com/nyc-tlc/trip+data/fhv_tripdata_"
file_type = "csv"

## test of config parameters
try:
    test = int(year_start)
    test = int(year_end)
except:
    logging.warning("Wrong parameters used! Must be integer. Exit.")
    sys.exit(1)

## Generate list of links
link_list = link_generator(link_part,file_type)
logging.info("File list to download:")
logging.info(link_list)

## list of links -> list of saved files
file_list = []
for a in link_list:
    file = get_file(a)
    try:
        assert len(file) > 2
        file_list.append(file)
    except AssertionError:
        pass # since that error has been alarmed anyways

## Put all files into one final output file
assemble_data()

logging.info("Script finished.")
